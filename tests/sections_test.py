import test_database as td
from unittest import TestCase
from unittest.mock import patch
from api.routers import sections
from api.common.responses import NotFound, Forbidden


class Sections_Should(TestCase):

    def test_getSection_returnSection_whenUserHasAccess(self):
        section_id = 1
        mock_user = td.fake_user()
        mock_section = td.fake_section()

        with patch('api.services.sections_service.get_by_id', return_value=mock_section) as mock_get_by_id, \
                patch('api.services.sections_service.update_section_is_view') as mock_update_is_view:
            result = sections.get_section(id=section_id, current_user=mock_user)

        self.assertEqual(result, mock_section)
        mock_get_by_id.assert_called_once_with(id=section_id, current_user=mock_user)
        mock_update_is_view.assert_called_once_with(section_id, mock_user)

    def test_getSection_returnForbidden_whenSectionIsPremium(self):
        mock_user = td.fake_user()
        mock_response = td.fake_response(Forbidden,"Only subscribed users have access to sections from Premium courses. Please subscribe.", status_code=403)
        expected = mock_response

        with patch('api.services.sections_service.get_by_id', return_value='premium') as mock_get_by_id:
            result = sections.get_section(id=1, current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_get_by_id.assert_called_once()

    def test_getSection_returnNotFound_whenSectionNotFound(self):
        section_id = 1
        mock_user = td.fake_user()
        mock_section = None
        mock_response = td.fake_response(NotFound, 'Section with Id No. {id} not found.', status_code=404)
        expected = mock_response

        with patch('api.services.sections_service.get_by_id', return_value=mock_section) as mock_get_by_id, \
                patch('api.services.sections_service.update_section_is_view') as mock_update_is_view:
            result = sections.get_section(id=section_id, current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        mock_get_by_id.assert_called_once_with(id=section_id, current_user=mock_user)
        mock_update_is_view.assert_not_called()
