from unittest.mock import Mock
from api.data.models import (UserOut, CourseStatus, StudentWithCourse,Tag, UserRatedCourse,
                        TagWithCoursesResponseModel,SortByTagAttribute, HomePageInput,
                        SortType,CourseResponseModel, Course, CourseWithSectionsResponseModel,
                        CourseType, Section, CourseEditModel, CourseTeacherResponseModel, User)
from api.common.responses import Forbidden
from fastapi.security import OAuth2PasswordRequestForm

def fake_user(id=1, username='student@abv.bg', role='student'):
   mock_user = Mock(spec=UserOut)
   mock_user.id = id
   mock_user.username = username
   mock_user.role = role
   return mock_user

def fake_course_status(status = 'Active'):
    mock_course_status = Mock(spec=CourseStatus)
    mock_course_status.status = status 
    return mock_course_status

def fake_student_with_course(id=1, username='student@gmail.com', name='Ivan Dimitrov', course='Python'):
    mock_student_with_course = Mock(spec=StudentWithCourse)
    mock_student_with_course.id = id
    mock_student_with_course.username = username
    mock_student_with_course.name =  name
    mock_student_with_course.course = course
    return mock_student_with_course

def fake_tag(id=1, area='Tag area'):
    mock_tag = Mock(spec=Tag)
    mock_tag.id = id
    mock_tag.area = area
    return mock_tag

def fake_course_type():
    mock_course_type = Mock(spec=CourseType)
    mock_course_type.Premium = 'Premium'
    mock_course_type.Public = 'Public'
    return mock_course_type

def fake_course_response_model(id=1,
                               title='Title',
                               description='Great Course',
                               objectives='Learn to code',
                               is_public: fake_course_type = 'Public',
                               teacher='Ivan Dimitrov',
                               rating= 8.00):
    mock_course_response_model=Mock(spec=CourseResponseModel)
    mock_course_response_model.id = id
    mock_course_response_model.title = title
    mock_course_response_model.description = description
    mock_course_response_model.objectives = objectives
    mock_course_response_model.is_public = is_public
    mock_course_response_model.teacher = teacher
    mock_course_response_model.rating = rating
    return mock_course_response_model

def fake_tag_with_courses_response_model(tag=None, courses=None):
    mock_tag_with_courses = Mock(spec=TagWithCoursesResponseModel)
    mock_tag_with_courses.tag = tag
    mock_tag_with_courses.courses = courses
    return mock_tag_with_courses

def fake_response(exact_response, content, status_code=0):
    mock_response = Mock(spec=exact_response)
    mock_response.status_code = status_code
    mock_response.content = content
    return mock_response

def fake_section(id=1,
                 title='Section 1',
                 content='Section content',
                 description='Great description!',
                 ext_link='www.google.com/?search=section1'):
    mock_section = Mock(spec=Section)
    mock_section.id=id
    mock_section.title=title
    mock_section.content=content
    mock_section.description=description
    mock_section.ext_link=ext_link
    return mock_section

def fake_course_with_sections_response(course: fake_course_response_model,
                                       sections = list[fake_section]):
    
    mock_course_with_sections = Mock(spec=CourseWithSectionsResponseModel)
    mock_course_with_sections.course = course
    mock_course_with_sections.sections = sections
    return mock_course_with_sections

def fake_course_edit_model(title='Title', description='Description',
                           objectives='Objectives', is_public='Public'):
    mock_course_edit_model = Mock(spec=CourseEditModel)
    mock_course_edit_model.title = title
    mock_course_edit_model.description = description
    mock_course_edit_model.objectives = objectives
    mock_course_edit_model.is_public = is_public
    return mock_course_edit_model

def fake_home_page(url_address='www.facebook.com'):
    mock_home_page = Mock(spec=HomePageInput)
    mock_home_page.url_address = url_address
    return mock_home_page

def fake_user_rated_course(rate=5,
                           id=1,
                           first_name='Ivan',
                           last_name='Dimitrov'):
    mock_user_rated_course = Mock(spec=UserRatedCourse)
    mock_user_rated_course.rate = rate
    mock_user_rated_course.id = id
    mock_user_rated_course.first_name = first_name
    mock_user_rated_course.last_name = last_name
    return mock_user_rated_course

def fake_course_teacher_response(course_id=1,
                                 course_title='Title',
                                 course_description='Great description',
                                 teacher_id=1):
    mock_course_teacher_response = Mock(spec=CourseTeacherResponseModel)
    mock_course_teacher_response.course_id = course_id
    mock_course_teacher_response.course_title = course_title
    mock_course_teacher_response.course_description = course_description
    mock_course_teacher_response.teacher_id = teacher_id
    return mock_course_teacher_response

def fake_course(title='Title',
            description='Great description',
            objectives='Objectives',
            is_public='Public'):
    mock_course = Mock(spec=Course)
    mock_course.title = title
    mock_course.description = description
    mock_course.objectives = objectives
    mock_course.is_public = is_public
    return mock_course

def fake_user_create(role= 'teacher', username='realperson@gmail.com',
                     first_name= 'Real', last_name='Person', password='Abv!45555',
                     phone_number='0123456789', linkedin_account='linkedin.com/real-person'):
    
    mock_user_create = Mock(spec=User)
    mock_user_create.role = role
    mock_user_create.username = username
    mock_user_create.first_name = first_name
    mock_user_create.last_name = last_name
    mock_user_create.password = password
    mock_user_create.phone_number = phone_number
    mock_user_create.linkedin_account = linkedin_account
    return mock_user_create

def fake_OAuth2(username='test_user', password='password'):
    mock_OAuth2 = Mock(spec=OAuth2PasswordRequestForm)
    mock_OAuth2.username = username
    mock_OAuth2.password = password
    return mock_OAuth2