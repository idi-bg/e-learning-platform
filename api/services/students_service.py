from api.data.database import read_query, update_query
from api.data.models import StudentWithCourse, SortType, SortStudentsBy, UserOut


def sort_students(lst: list[StudentWithCourse], *, attribute = 'id', reverse=False) -> list[StudentWithCourse]:
    '''Sorts list of Students.'''
    if attribute == 'username':
        def sort_by(u: StudentWithCourse): return u.username
    elif attribute == 'course':
        def sort_by(u: StudentWithCourse): return u.course
    elif attribute == 'name':
        def sort_by(u: StudentWithCourse): return u.name
    else:
        def sort_by(u: StudentWithCourse): return u.id
        
    return sorted(lst, key=sort_by, reverse=reverse)


def teacher_report_for_subscribed_students(current_user: UserOut,
                                        search: str | None = None,
                                        sort: SortType | None = None,
                                        sort_by: SortStudentsBy | None = None,
                                        get_data_func = None) -> list[StudentWithCourse]:
    '''Returns Teacher report with a list of past and current students that have subscribed for the courses of the respective teacher.'''
    
    if get_data_func is None:
        get_data_func = read_query

    if current_user.role == 'teacher':
        sql = '''SELECT u.id, u.email AS username, CONCAT(u.f_name, ' ', u.l_name) as student, c.title as course  
                FROM users_has_courses AS uhc 
                LEFT JOIN users AS u ON u.id = uhc.user_id
                LEFT JOIN courses AS c ON uhc.course_id = c.id     
                WHERE c.teacher_id = ?'''

        where_clauses = []
        if search:
            sql += " AND CONCAT(u.f_name, ' ', u.l_name) LIKE ?"
            where_clauses.append(f'%{search}%')

        data = get_data_func(sql, (current_user.id, *where_clauses))

        courses = list(StudentWithCourse.from_query_result(*row) for row in data)
        
        if (sort and (sort == 'asc' or sort == 'desc'))or sort_by:
            courses = sort_students(lst=courses, attribute=sort_by, reverse=sort == 'desc')
        
        return courses


def subscription_approval(student_id: int, course_id: int, current_user: UserOut, update_data_func = None) -> bool | str:
    '''Admin or relevant Teacher can approve subscription (access) to course'''
    if update_data_func is None:
        update_data_func = update_query

    params = [student_id, course_id]

    sql = '''UPDATE users_has_courses AS uhc
            JOIN courses AS c ON uhc.course_id = c.id
            SET uhc.has_access = CASE
                WHEN uhc.has_access = 0 THEN 1 
                ELSE uhc.has_access
            END
        WHERE uhc.user_id = ? AND uhc.course_id = ?'''
    
    if current_user.role == 'teacher':
        sql += ' AND c.teacher_id = ?'
        params.append(current_user.id)

    affected_rows = update_data_func(sql, tuple(params))
    
    return True if affected_rows == 1 else False


def remove_student_from_course(student_id: int, course_id: int, current_user: UserOut, update_data_func = None) -> bool | str:
    '''Only Admin could remote students from courses.'''
    if update_data_func is None:
        update_data_func = update_query
    
    if current_user.role == 'admin':

        sql = '''DELETE uhc FROM users_has_courses AS uhc 
                WHERE (course_id = ? AND user_id = ?)'''
                
        affected_rows = update_data_func(sql,(course_id, student_id))
        
        return True if affected_rows == 1 else False
