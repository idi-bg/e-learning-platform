from mariadb import IntegrityError
from api.data.database import read_query, insert_query
from api.data.models import Section, SortBySectionAttribute, SortType, UserOut


def create_section(section: Section, insert_data_func=None):
    """Inserts new Section into Database"""
    if insert_data_func is None:
        insert_data_func = insert_query

    data = insert_data_func(
        "INSERT INTO sections(title,content,description,ext_link) VALUES (?, ?,?,?)",
        (section.title, section.content, section.description, section.ext_link),
    )
    section.id = data

    return section


def get_id_title(title: str, get_data_func=None) -> Section:
    """Returns Section ID, found by Title."""
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func("SELECT id FROM sections WHERE title = ?", (title,))

    if data:
        return data

    return None


def get_by_title(title: str, get_data_func=None) -> Section:
    """Returns Section, found by Title."""
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func("SELECT * FROM sections WHERE title = ?", (title,))

    return next((Section.from_query_result(*row) for row in data), None)


def get_by_id(id: int, current_user: UserOut, get_data_func=None) -> Section:
    """Returns Section, found by ID."""
    if get_data_func is None:
        get_data_func = read_query

    if current_user.role == "student":
        data = get_data_func(
            """SELECT 
                        CASE
                            WHEN c.is_public = 1 THEN CONCAT_WS('|^', s.id, s.title, s.content, s.description, s.ext_link)
                            WHEN uhc.has_access = 1 AND uhc.user_id = ? THEN CONCAT_WS('|^', s.id, s.title, s.content, s.description, s.ext_link)
                            WHEN s.id IS NULL THEN '[]'
                            ELSE CONCAT_WS('|^', NULL)
                        END AS section
                    FROM courses AS c 
                    LEFT JOIN courses_has_sections AS chs ON chs.course_id = c.id
                    LEFT JOIN sections AS s ON chs.section_id = s.id
                    LEFT JOIN users_has_courses AS uhc ON c.id = uhc.course_id AND uhc.user_id = ?
                    LEFT JOIN course_status AS cs ON c.id = cs.course_id
                    WHERE s.id = ? AND (cs.is_active = 1 OR cs.is_active IS NULL)""",
            (current_user.id, current_user.id, id),
        )
    else:
        data = get_data_func(  # Access always allowed to Admins / Teachers
            """SELECT CASE
                    WHEN c.is_public in (1,0) THEN CONCAT_WS('|^', s.id, s.title, s.content, s.description, s.ext_link)
                    WHEN s.id IS NULL THEN '[]'
                    ELSE CONCAT_WS('|^', NULL)
                    END AS section
                    FROM courses AS c 
                    LEFT JOIN courses_has_sections AS chs ON chs.course_id = c.id
                    LEFT JOIN sections AS s ON chs.section_id = s.id
                    WHERE s.id = ?""",
            (id,),
        )

    if data == []:
        return None  # Not Found
    elif not data[0][0]:
        return "premium"  # Forbidden
    else:
        data = data[0][0].split("|^")
        return Section.from_query_result(*data)


def sort_sections(
    lst: list[Section], *, attribute="title", reverse=False
) -> list[Section]:
    """Sorts list of Sections."""
    if attribute == "title":

        def sort_by(section: Section):
            return section.title

    else:

        def sort_by(section: Section):
            return section.id

    return sorted(lst, key=sort_by, reverse=reverse)


def all_sections(
    sort: SortType | None = None,
    sort_by: SortBySectionAttribute | None = None,
    current_user: UserOut = None,
    get_data_func=None,
) -> list[Section]:
    """Returns a list of all sections (if admin/teacher), elif user: only to public or premium courses where user has subscription."""
    if get_data_func is None:
        get_data_func = read_query

    sql = """SELECT s.id, s.title, s.content, s.description, s.ext_link
                FROM courses AS c 
                LEFT JOIN courses_has_sections AS chs ON chs.course_id = c.id
                LEFT JOIN sections AS s ON chs.section_id = s.id
                LEFT JOIN course_status AS cs ON c.id = cs.course_id"""

    params = ()
    if current_user.role == "student":
        sql += """ LEFT JOIN users_has_courses AS uhc ON c.id = uhc.course_id AND uhc.user_id = ?
                WHERE (c.is_public = 1 OR (uhc.has_access = 1 AND uhc.user_id = ?)) AND (cs.is_active = 1 OR cs.is_active IS NULL)"""
        params = (current_user.id, current_user.id)
    else:
        sql += """ LEFT JOIN users_has_courses AS uhc ON c.id = uhc.course_id
                WHERE c.is_public in (1,0)"""

    sql += """ AND CONCAT_WS('|^', s.id, s.title, s.content, s.description, s.ext_link) IS NOT NULL
      AND CONCAT_WS('|^', s.id, s.title, s.content, s.description, s.ext_link) != ''
      GROUP BY s.id"""

    data = get_data_func(sql, params)

    sections_in_course = list(Section.from_query_result(*row) for row in data)

    if sort:
        sections_in_course = sort_sections(
            sections_in_course, attribute=sort_by, reverse=sort == "desc"
        )

    return sections_in_course


def update_section_is_view(id: int, current_user: UserOut, get_data_func=None):
    if get_data_func == None:
        get_data_func = insert_query

        get_data_func(
            "INSERT INTO users_has_sections (is_viewed, users_id, sections_id)VALUES (?, ?, ?)ON DUPLICATE KEY UPDATE is_viewed = IF(is_viewed = VALUES(is_viewed) AND users_id = VALUES(users_id) AND sections_id = VALUES(sections_id), VALUES(is_viewed), is_viewed)",
            (1, current_user.id, id),
        )
