from pydantic import conint, constr
from fastapi_pagination import Page, paginate
from api.common.responses import NotFound, Created
from api.services import users_service, admins_service
from fastapi import APIRouter, Depends, Query, Path
from api.data.models import (
    SortType,
    UserOut,
    AdminReportResponseModel,
    CourseStatus,
    UserStatus,
)


admins_router = APIRouter(prefix="/admins", tags=["Admins"])


@admins_router.get(
    "/reports", status_code=200, response_model=Page[AdminReportResponseModel]
)
def get_admin_report_for_courses(
    search: constr(strip_whitespace=True, min_length=1, max_length=30) | None = None,
    sort: SortType | None = None,
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Returns a list with all public and premium courses, the number of students in them and their rating. Can be accessed only by Admin users."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="admin",
        message="Only Admins could access the report for courses.",
    )
    if is_forbidden:
        return is_forbidden

    return paginate(
        admins_service.admin_report_for_courses(
            search=search, sort=sort, current_user=current_user
        )
    )


@admins_router.put("/courses/{id}", status_code=201)
def update_course_active_status(
    id: conint(ge=1) = Path(),
    course_status: CourseStatus = Query(),
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Admins can deactivate/activate courses. If the course has been deactivated, the enrolled students will receive an email notifications about the change.
    Can be accessed only by Admin users."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="admin",
        message="Only Admins could activate / deactivate courses.",
    )
    if is_forbidden:
        return is_forbidden

    updated_course = admins_service.update_course_active_status(
        id=id, course_status=course_status, current_user=current_user
    )

    if updated_course == "Not found":
        return NotFound(content=f"Course with ID No. {id} does not exist.")
    elif updated_course:
        if course_status == "Inactive":
            return Created(
                content=f'Course status successfully changed to "Inactive". Email notifications are sent to each enrolled students.'
            )
        else:
            return Created(content=f'Course status successfully changed to "Active".')
    else:
        if course_status == "Inactive":
            return Created(content=f'Course status successfully changed to "Inactive".')
        else:
            return Created(content=f'Course status successfully changed to "Active".')


@admins_router.put("/users/{id}", status_code=201)
def grant_or_revoke_student_access(
    id: conint(ge=1) = Path(),
    user_status: UserStatus = Query(),
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Admin can approve or revoke User access."""
    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="admin",
        message="Only Admins can activate / deactivate users.",
    )
    if is_forbidden:
        return is_forbidden
    return admins_service.change_user_access(id, user_status)
