from enum import Enum
from typing import Optional
from pydantic import BaseModel, EmailStr, conint, constr, HttpUrl, confloat


class CourseType(str, Enum):
    Public = "Public"
    Premium = "Premium"


class Course(BaseModel):
    title: constr(strip_whitespace=True, min_length=2, max_length=100)
    description: constr(strip_whitespace=True, min_length=10, max_length=2000)
    objectives: constr(strip_whitespace=True, min_length=10, max_length=2000)
    is_public: CourseType

    @classmethod
    def from_query_result(
        cls, title: str, description: str, objectives: str, is_public: int
    ):
        return cls(
            title=title,
            description=description,
            objectives=objectives,
            is_public=("Public" if is_public in ("1", 1) else "Premium"),
        )


class CourseResponseModel(BaseModel):
    id: conint(ge=1) | None
    title: constr(strip_whitespace=True, min_length=2, max_length=100)
    description: constr(strip_whitespace=True, min_length=10, max_length=2000)
    objectives: constr(strip_whitespace=True, min_length=10, max_length=2000)
    is_public: CourseType
    teacher: str
    rating: confloat(ge=0) = 0.0

    @classmethod
    def from_query_result(
        cls,
        id: int,
        title: str,
        description: str,
        objectives: str,
        is_public: int,
        teacher: str,
        rating: float = 0.0,
    ):
        return cls(
            id=id,
            title=title,
            description=description,
            objectives=objectives,
            is_public=("Public" if is_public in ("1", 1) else "Premium"),
            teacher=teacher,
            rating=rating if rating else 0,
        )


class Section(BaseModel):
    id: conint(ge=1) | None = None
    title: constr(strip_whitespace=True, min_length=2, max_length=100)
    content: constr(strip_whitespace=True, min_length=2, max_length=100)
    description: constr(
        strip_whitespace=True, min_length=10, max_length=2000
    ) | None = None
    ext_link: constr(
        strip_whitespace=True, min_length=10, max_length=2000
    ) | None = None

    @classmethod
    def from_query_result(
        cls,
        id,
        title: str,
        content: str,
        description: str | None = None,
        ext_link: str | None = None,
    ):
        return cls(
            id=id,
            title=title,
            content=content,
            description=description,
            ext_link=ext_link,
        )


class Roles(str, Enum):
    student = "student"
    teacher = "teacher"


class RolesOut(str, Enum):
    student = "student"
    teacher = "teacher"
    admin = "admin"


TPassword = constr(
    regex=r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W).{8,30}$", strip_whitespace=True
)


class User(BaseModel):
    id: int | None = None
    role: Roles | None = None
    username: EmailStr | None = None
    first_name: constr(strip_whitespace=True, min_length=2, max_length=20) | None = None
    last_name: constr(strip_whitespace=True, min_length=3, max_length=20) | None = None
    password: TPassword | None = None
    phone_number: constr(strip_whitespace=True, regex = r"^[+\d\s]{8,15}$") | None = None
    linkedin_account: constr(
        regex=r"^(linkedin\.com\/)[a-z\d]+(-[a-z\d]+)*$", strip_whitespace=True
    ) | None = None


class ViewTeacher(BaseModel):
    role: Roles
    username: EmailStr
    first_name: constr(strip_whitespace=True, min_length=2, max_length=20)
    last_name: constr(strip_whitespace=True, min_length=3, max_length=20)
    phone_number: constr(regex=r"^\d{7,15}$", strip_whitespace=True)
    linkedin_account: constr(
        regex=r"^(linkedin\.com\/)[a-z\d]+(-[a-z\d]+)*$", strip_whitespace=True
    )

    @classmethod
    def from_query_result(
        cls, email, first_name, last_name, phone_number, linkedin_account, role = "teacher"
    ):
        return cls(
            role=role,
            username=email,
            first_name=first_name,
            last_name=last_name,
            phone_number=phone_number,
            linkedin_account=linkedin_account,
        )


class SortStudentsBy(str, Enum):
    id = "id"
    username = "username"
    name = "name"
    course = "course"


class ViewStudent(BaseModel):
    role: Roles
    username: EmailStr
    first_name: constr(strip_whitespace=True, min_length=2, max_length=20)
    last_name: constr(strip_whitespace=True, min_length=3, max_length=20)

    @classmethod
    def from_query_result(cls, role, email, first_name, last_name):
        return cls(
            role="student", username=email, first_name=first_name, last_name=last_name
        )


class StudentWithCourse(BaseModel):
    id: conint(ge=1)
    username: EmailStr
    name: constr(strip_whitespace=True, min_length=2, max_length=100)
    course: constr(strip_whitespace=True, min_length=2, max_length=100)

    @classmethod
    def from_query_result(cls, id, email, name, course):
        return cls(id=id, username=email, name=name, course=course)


class UserOut(BaseModel):
    id: int
    username: str
    role: RolesOut

    @classmethod
    def from_query_result(cls, id, username, role):
        return cls(
            id=id,
            username=username,
            role=("teacher" if role == 1 else ("student" if role == 0 else "admin")),
        )


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Optional[str] = None


class UpdatePassword(BaseModel):
    password: TPassword


class Tag(BaseModel):
    id: conint(ge=1) | None = None
    area: constr(strip_whitespace=True, min_length=2, max_length=30)

    @classmethod
    def from_query_result(cls, id: int, area: str):
        return cls(id=id, area=area)


class SortBySectionAttribute(str, Enum):
    id = "id"
    title = "title"


class SortByTagAttribute(str, Enum):
    id = "id"
    area = "area"


class SortType(str, Enum):
    asc = "asc"
    desc = "desc"


class CourseWithSectionsResponseModel(BaseModel):
    course: CourseResponseModel
    sections: list[Section]

    @classmethod
    def from_query_result(cls, course: Course, sections: list[Section]):
        return cls(course=course, sections=sections)


class TagWithCoursesResponseModel(BaseModel):
    tag: Tag
    courses: list[CourseResponseModel]

    @classmethod
    def from_query_result(cls, tag: Tag, courses: list[Course]):
        return cls(tag=tag, courses=courses)


class HomePageInput(BaseModel):
    url_address: HttpUrl


class CourseEditModel(BaseModel):
    title: constr(strip_whitespace=True, min_length=2, max_length=100) | None = None
    description: constr(
        strip_whitespace=True, min_length=10, max_length=2000
    ) | None = None
    objectives: constr(
        strip_whitespace=True, min_length=10, max_length=2000
    ) | None = None
    is_public: CourseType | None = None

    def from_query_result(
        cls,
        title: str,
        description: str,
        objectives: str,
        is_public,
    ):
        kwargs = {
            "description": description,
            "objectives": objectives,
            "is_public": "Public" if is_public == 1 else "Premium",
        }

        if title is not None:
            kwargs["title"] = title

        return cls(**kwargs)


class UserRatedCourse(BaseModel):
    rate: int
    id: int
    first_name: str
    last_name: str

    @classmethod
    def from_query_result(cls, rate: int, id: int, first_name: str, last_name: str):
        return cls(rate=rate, id=id, first_name=first_name, last_name=last_name)


class CourseTeacherStudentResponseModel(BaseModel):
    user_id: int
    course_id: int
    course_title: str
    course_description: str
    teacher_id: int

    @classmethod
    def from_query_result(
        cls,
        user_id: int,
        course_id: int,
        course_title: str,
        course_description: str,
        teacher_id: int,
    ):
        return cls(
            user_id=user_id,
            course_id=course_id,
            course_title=course_title,
            course_description=course_description,
            teacher_id=teacher_id,
        )


class CourseTeacherResponseModel(BaseModel):
    course_id: int
    course_title: str
    course_description: str
    teacher_id: int

    @classmethod
    def from_query_result(
        cls, course_id: int, course_title: str, course_description: str, teacher_id: int
    ):
        return cls(
            course_id=course_id,
            course_title=course_title,
            course_description=course_description,
            teacher_id=teacher_id,
        )


class AdminReportResponseModel(BaseModel):
    id: conint(ge=1) | None
    title: constr(strip_whitespace=True, min_length=2, max_length=100)
    is_public: CourseType
    rating: confloat(ge=0) = 0.0
    enrolled_students: conint(ge=0) = 0

    @classmethod
    def from_query_result(
        cls,
        id: int,
        title: str,
        is_public: int,
        rating: float = 0.0,
        enrolled_students: int = 0,
    ):
        return cls(
            id=id,
            title=title,
            is_public=("Public" if is_public == 1 else "Premium"),
            rating=rating if rating else 0,
            enrolled_students=enrolled_students if enrolled_students else 0,
        )


class CourseStatus(str, Enum):
    Active = "Active"
    Inactive = "Inactive"


class UserStatus(str, Enum):
    Active = "Active"
    Inactive = "Inactive"
