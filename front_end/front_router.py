from fastapi import FastAPI, APIRouter, Depends, Request, HTTPException, Form
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates
from api.services import courses_service, users_service, sections_service
from api.data.models import UserOut
import os
import requests
from starlette.datastructures import FormData
from typing import Optional
import json

app = FastAPI()


current_directory = os.path.dirname(os.path.abspath(__file__))
template_directory = os.path.join(current_directory, "templates")
templates = Jinja2Templates(directory=template_directory)

front_router = APIRouter(prefix="/poodle", tags=["html"])


def validate_authentication(request: Request):
    """Check if there is a token in the header then validate it and return current user"""
    cookie_header = request.headers.get("cookie")
    if cookie_header:
        try:
            token = cookie_header[22:-1]
            user = users_service.get_current_user(token=token)

            return user
        except HTTPException:
            error_message = "Sesion expired please login again"
            return templates.TemplateResponse(
                "login.html", {"request": request, "error_message": error_message}
            )
    courses = courses_service.get_public_courses_for_guests()
    return templates.TemplateResponse(
        "home.html", {"request": request, "courses": courses, "current_user": None}
    )


@front_router.get("/", response_class=HTMLResponse)
def index(request: Request):
    user = validate_authentication(request=request)
    if isinstance(user, UserOut):
        courses = courses_service.all(current_user=user)
        return templates.TemplateResponse(
            "home.html", {"request": request, "courses": courses, "current_user": user}
        )
    else:
        return user


@front_router.get("/login", response_class=HTMLResponse)
def login(request: Request):
    return templates.TemplateResponse("login.html", {"request": request})


@front_router.post("/login")
def login(request: Request, form_data: OAuth2PasswordRequestForm = Depends()):
    real_password = users_service.get_password(form_data.username)
    
    try:
        is_valid = users_service.compare_passwords(form_data.password, real_password)
    except:
        return templates.TemplateResponse(
            "login.html", {"request": request, "error_message": "Invalid credentials"}
        )
    if not is_valid or not real_password:
        error_message = "Invalid credentials"
        return templates.TemplateResponse(
            "login.html", {"request": request, "error_message": error_message}
        )

    access_token = users_service.create_access_token(data={"email": form_data.username})

    try:
        current_user = users_service.get_current_user(access_token)
    except HTTPException as e:
        error_message = repr(e)
        if (
            error_message
            == "HTTPException(status_code=401, detail='User is not verified')"
        ):
            error_message = (
                "Please confirm your account with the email that we sent to you"
            )
        elif (
            error_message
            == "HTTPException(status_code=401, detail='User is deactivated')"
        ):
            error_message = "Your account has been deactivated. Please check your email for more information."

        return templates.TemplateResponse(
            "login.html", {"request": request, "error_message": error_message}
        )

    if current_user:
        courses = courses_service.all(current_user=current_user)

        response = templates.TemplateResponse(
            "home.html",
            {"request": request, "current_user": current_user, "courses": courses},
        )

        response.set_cookie("Authorization", f"Bearer {access_token}", httponly=True)

        return response


@front_router.get("/courses/{course_id}/")
def course(request: Request, course_id: int):
    user = validate_authentication(request=request)

    if isinstance(user, UserOut):
        course = courses_service.get_by_id(current_user=user, id=course_id)
        progress = courses_service.course_progress(id=course_id, current_user=user)
        progress = progress["result"]
        if user.role == "teacher":
            role = 1
        elif user.role == "admin":
            role = 2
        else:
            role = 0

        if course == False:
            return templates.TemplateResponse(
                "current_course.html",
                {
                    "request": request,
                    "current_user": user,
                    "course": None,
                    "current_id": course_id,
                    "progress": progress,
                },
            )
        else:
            # check if the current logged user is owner of the course
            is_owner = courses_service.update_course(
                update_data_func=None, course_id=course_id, current_user=user
            )
            if is_owner == "You are not the teacher of this course.":
                # not owner
                is_owner = False
                return templates.TemplateResponse(
                    "current_course.html",
                    {
                        "request": request,
                        "current_user": user,
                        "course": course,
                        "current_id": course_id,
                        "progress": progress,
                        "role": role,
                        "is_owner": is_owner,
                    },
                )
            elif is_owner is None:
                # owner
                is_owner = True
                return templates.TemplateResponse(
                    "current_course.html",
                    {
                        "request": request,
                        "current_user": user,
                        "course": course,
                        "current_id": course_id,
                        "progress": progress,
                        "role": role,
                        "is_owner": is_owner,
                    },
                )
        return templates.TemplateResponse(
            "current_course.html",
            {
                "request": request,
                "current_user": user,
                "course": course,
                "current_id": course_id,
                "progress": progress,
                "role": role,
            },
        )
    else:
        error_message = "Please login to be able to view courses"
        return templates.TemplateResponse(
            "login.html", {"request": request, "error_message": error_message}
        )


@front_router.get("/logout")
def logout(request: Request):
    courses = courses_service.get_public_courses_for_guests()
    response = templates.TemplateResponse(
        "home.html", {"request": request, "courses": courses}
    )
    response.delete_cookie(key="Authorization")
    return response


@front_router.get("/courses/sections/{section_id}/")
def section(request: Request, section_id: int):
    user = validate_authentication(request=request)
    if isinstance(user, UserOut):
        # go from touter
        section = sections_service.get_by_id(current_user=user, id=section_id)
        is_viewed = sections_service.update_section_is_view(section_id, user)
        if section == "premium":
            return templates.TemplateResponse(
                "current_course.html",
                {"request": request, "current_user": user, "section": "premium"},
            )
        else:
            return templates.TemplateResponse(
                "current_section.html",
                {"request": request, "current_user": user, "section": section},
            )
    else:
        return user


@front_router.get("/users")
def view_currrent_user_info(request: Request):
    user = validate_authentication(request=request)

    if isinstance(user, UserOut):
        current_user_info = users_service.view_info(user_auth=user)
        print(current_user_info)

        response = templates.TemplateResponse(
            "profile.html", {"request": request, "current_user": current_user_info}
        )

    return response


@front_router.post("/users")
def update_user_info(
    request: Request, first_name: str = Form(...), last_name: str = Form(...)
):
    user = validate_authentication(request=request)
    url = "http://127.0.0.1:5000/api/users"
    cookie_header = request.headers.get("cookie")
    token = cookie_header[22:-1]
    user_update = {}
    if first_name:
        user_update["first_name"] = first_name
    if last_name:
        user_update["last_name"] = last_name

    

    headers = {"Authorization": f"Bearer {token}"}
    response = requests.put(url, json=user_update, headers=headers)
    if response.status_code == 422:
        message = "An error occurred while updating the information."
    else:
        message = "Information updated successfuly."
    return templates.TemplateResponse(
        "profile.html", {"request": request, "current_user": user, "message": message}
    )


@front_router.post("/courses/subscribe/{course_id}")
def subsribe_for_course(request: Request, course_id: int):
    user = validate_authentication(request=request)

    url = f"http://127.0.0.1:5000/api/courses/{course_id}"
    cookie_header = request.headers.get("cookie")
    token = cookie_header[22:-1]

    headers = {"Authorization": f"Bearer {token}"}
    response = requests.put(url, headers=headers)
    if response.status_code == 201:
        message = response.text
    else:
        message = response.text
    return templates.TemplateResponse(
        "message.html", {"request": request, "current_user": user, "message": message}
    )


@front_router.post("/courses/unsubscribe/{course_id}")
def unsubsribe_for_course(request: Request, course_id: int):
    user = validate_authentication(request=request)

    url = f"http://127.0.0.1:5000/api/courses/{course_id}"
    cookie_header = request.headers.get("cookie")
    token = cookie_header[22:-1]

    headers = {"Authorization": f"Bearer {token}"}
    response = requests.delete(url, headers=headers)
    if response.status_code == 204:
        message = "Unsubscribe successfull"
    else:
        message = response.text
    return templates.TemplateResponse(
        "message.html", {"request": request, "current_user": user, "message": message}
    )


@front_router.get("/register")
def register_user(request: Request):
    return templates.TemplateResponse("register.html", {"request": request})


@front_router.post("/register")
def register_user(
    request: Request,
    email: str = Form(...),
    first_name: str = Form(...),
    last_name: str = Form(...),
    password: str = Form(...),
    role: str = Form(...),
    phone_number: Optional[str] = Form(None),
    linkedin_account: Optional[str] = Form(None),
):
    if role == "teacher":
        phone_number = phone_number or ""
        linkedin_account = linkedin_account or ""

    user_data = {
        "username": email,
        "first_name": first_name,
        "last_name": last_name,
        "password": password,
        "role": role,
        "phone_number": phone_number,
        "linkedin_account": linkedin_account,
    }

    response = requests.post("http://127.0.0.1:5000/api/guests/signup", json=user_data)

    if response.status_code == 422:
        response_dict = json.loads(response.text)

        message = response_dict["detail"][0]["msg"]
        print(message)
        if (
            message
            == 'string does not match regex "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W).{8,30}$"'
        ):
            return templates.TemplateResponse(
                "message.html",
                {
                    "request": request,
                    "message": "Password must have upper letter, special letter and numbers.",
                },
            )
        elif message == 'string does not match regex "^\d{8,15}$"':
            return templates.TemplateResponse(
                "message.html",
                {
                    "request": request,
                    "message": "Phone number should be between 8-15 symbols",
                },
            )
        else:
            return templates.TemplateResponse(
                "message.html",
                {
                    "request": request,
                    "message": "Enter valid linkedin link.",
                },
            )

    message = response.text
    return templates.TemplateResponse(
        "message.html", {"request": request, "message": message}
    )


@front_router.post("/teacher/create/course")
def create_course(
    request: Request,
    title: str = Form(...),
    description: str = Form(...),
    objectives: str = Form(...),
    is_public: str = Form(...),
):
    current_user = validate_authentication(request=request)
    cookie_header = request.headers.get("cookie")
    token = cookie_header[22:-1]

    headers = {"Authorization": f"Bearer {token}"}

    if isinstance(current_user, UserOut):
        user_data = {
            "title": title,
            "description": description,
            "objectives": objectives,
            "is_public": is_public,
        }

        response = requests.post(
            "http://127.0.0.1:5000/api/courses", json=user_data, headers=headers
        )

        if response.status_code == 201:
            message = "Successfully created course"
            return templates.TemplateResponse(
                "teacher_create_course.html",
                {"request": request, "current_user": current_user, "message": message},
            )
        elif response.status_code == 409:
            error = "Title already in use!"
            return templates.TemplateResponse(
                "teacher_create_course.html",
                {"request": request, "current_user": current_user, "error": error},
            )
        else:
            error = "Description and Objectives:(min 10 characters)."
            return templates.TemplateResponse(
                "teacher_create_course.html",
                {"request": request, "current_user": current_user, "error": error},
            )

    else:
        return current_user


@front_router.get("/teacher")
def view_create_course(request: Request):
    user = validate_authentication(request=request)
    if isinstance(user, UserOut):
        return templates.TemplateResponse(
            "teacher_create_course.html", {"request": request, "current_user": user}
        )


@front_router.post("/courses/rates/{course_id}")
def user_rate_course(request: Request, course_id: int, rate: int = Form(...)):
    user = validate_authentication(request=request)

    url = f"http://127.0.0.1:5000/api/courses/{course_id}/rates?rate={rate}"
    cookie_header = request.headers.get("cookie")
    token = cookie_header[22:-1]

    headers = {"Authorization": f"Bearer {token}"}
    user_data = {"rate": rate}
    response = requests.put(url, user_data, headers=headers)

    if response.status_code == 201:
        message = "Rate successful"
        return templates.TemplateResponse(
            "message.html",
            {"request": request, "current_user": user, "message": message},
        )
    elif response.text == "User does not have access to the course.":
        return templates.TemplateResponse(
            "message.html",
            {"request": request, "current_user": user, "message": response.text},
        )
    elif response.text == "Only Students can rate courses.":
        return templates.TemplateResponse(
            "message.html",
            {"request": request, "current_user": user, "message": response.text},
        )
    elif response.text == "You can only give 1 rate per course.":
        return templates.TemplateResponse(
            "message.html",
            {"request": request, "current_user": user, "message": response.text},
        )
    elif response.text == "Course does not exist!":
        return templates.TemplateResponse(
            "message.html",
            {"request": request, "current_user": user, "message": response.text},
        )


@front_router.post("/teacher/edit/course/{id}")
def edit_course(
    request: Request,
    id: int,
    title: str = Form(...),
    description: str = Form(...),
    objectives: str = Form(...),
    is_public: str = Form(...),
):
    current_user = validate_authentication(request=request)
    cookie_header = request.headers.get("cookie")
    token = cookie_header[22:-1]

    headers = {"Authorization": f"Bearer {token}"}

    if isinstance(current_user, UserOut):
        user_data = {
            "title": title,
            "description": description,
            "objectives": objectives,
            "is_public": is_public,
        }

        response = requests.patch(
            f"http://127.0.0.1:5000/api/courses/{id}", json=user_data, headers=headers
        )

        if response.text == "Course not found.":
            error = response.text
            return templates.TemplateResponse(
                "teacher_create_course.html",
                {
                    "request": request,
                    "current_user": current_user,
                    "error": error,
                    "id": id,
                },
            )
        elif response.text == "You are not the teacher of this course.":
            error = response.text
            return templates.TemplateResponse(
                "teacher_edit_course.html",
                {
                    "request": request,
                    "current_user": current_user,
                    "error": error,
                    "id": id,
                },
            )
        elif response.text == "Cannot update the same information.":
            return templates.TemplateResponse(
                "teacher_edit_course.html",
                {
                    "request": request,
                    "current_user": current_user,
                    "error": response.text,
                    "id": id,
                },
            )

        elif response.status_code == 201:
            message = "Successfully edit course"
            return templates.TemplateResponse(
                "teacher_edit_course.html",
                {
                    "request": request,
                    "current_user": current_user,
                    "message": message,
                    "id": id,
                },
            )

    else:
        return current_user


@front_router.get("/teacher/edit/course/{id}")
def get_page_for_edit(request: Request, id: int):
    user = validate_authentication(request=request)
    if isinstance(user, UserOut):
        return templates.TemplateResponse(
            "teacher_edit_course.html",
            {"request": request, "current_user": user, "id": id},
        )


@front_router.post("/teacher/course/{id}/section")
def teacher_create_section(
    request: Request,
    id: int,
    title: str = Form(...),
    description: str = Form(...),
    content: str = Form(...),
):
    user = validate_authentication(request=request)

    url = f"http://127.0.0.1:5000/api/courses/{id}/sections"
    cookie_header = request.headers.get("cookie")
    token = cookie_header[22:-1]

    headers = {"Authorization": f"Bearer {token}"}
    user_data = {"title": title, "content": content, "description": description}

    response = requests.put(url, json=user_data, headers=headers)

    if response.status_code == 422:
        error = response.text
        return templates.TemplateResponse(
            "teacher_create_section.html.",
            {"request": request, "current_user": user, "error": error, "id": id},
        )
    elif response.status_code == 201:
        message = "Successfully create section"
        return templates.TemplateResponse(
            "teacher_create_section.html",
            {"request": request, "current_user": user, "message": message, "id": id},
        )


@front_router.get("/teacher/delete/section/{section_id}/{course_id}")
def delete_section(request: Request, section_id: int, course_id: int):
    user = validate_authentication(request=request)
    if isinstance(user, UserOut):
        cookie_header = request.headers.get("cookie")
        token = cookie_header[22:-1]

        headers = {"Authorization": f"Bearer {token}"}
        response = requests.delete(
            f"http://127.0.0.1:5000/api/courses/{course_id}/sections/{section_id}",
            headers=headers,
        )

        if response.status_code == 204:
            message = "Successfully delete section"
            return RedirectResponse(f"http://127.0.0.1:5000/poodle/courses/{course_id}")

    return user


@front_router.get("/teacher/course/{id}/section")
def get_page_for_edit_section(request: Request, id: int):
    user = validate_authentication(request=request)
    if isinstance(user, UserOut):
        return templates.TemplateResponse(
            "teacher_create_section.html",
            {"request": request, "current_user": user, "id": id},
        )
